<div class="container">
    <div class="search-wrapper">
        <input type="text" name="search" id="" class="search" placeholder="Searh by tags..." wire:model="search">
        <a href="#" class="create-btn" id="open-modal">Create note</a>
    </div>    

    <div class="notes">
            @foreach ($notes as $note)
                
                <div class="note">
                    <div class="tags">

                        @foreach ($note->tags() as $tag) 
                            <p>
                                <span 
                                    class="tag" 
                                    style="background-color: {{ $tag->color }}; 
                                            color: {{ $tag->text_color ? "black" : "white" }}">
                                    {{ $tag->name }}
                                </span>
                            </p>
                        @endforeach 

                    </div> 
                    <div class="text">
                        <p>{{ $note->text }}</p>
                    </div>
                </div>

            @endforeach 

    </div>
</div> 