<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="/css/app.css"> 
    @livewireStyles
    <title>Notes</title>
</head>
<body>

    @livewire('search-notes')

    <div class="create-modal" id="create-modal">
        <div class="modal-content">
            <form action="">
                <input type="text" name="name" id="" placeholder="text">
                <br>
                <select name="category" id="">
                    <option value="1">Cat 1</option>
                    <option value="2">Cat 2</option>
                    <option value="3">Cat 3</option>
                </select>
                <button type="submit" class="create-modal-btn">Create</button>
            </form>
        </div> 
    </div>

@livewireScripts 
<script src="/js/main.js"></script>
</body>
</html>