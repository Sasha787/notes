<?php

namespace App\Http\Livewire;

use Livewire\Component;
use App\Models\Note;
class SearchNotes extends Component
{
    public $search = '';

    public function render()
    {
        return view('livewire.search-notes', [
            'notes' => Note::getByTags($this->search),
        ]);
    }
}
