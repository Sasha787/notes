<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Models\Tag;
use App\Models\Note;
use App\Models\Tag_Note;
use App\Models\User_Team;
use App\Models\Team;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// Вывод всех тегов

Route::get('/tags', function() {
    return response(Tag::all());
});

// Добавление тегов

Route::post('/tag/add', function(Request $request) {
    $tag = Tag::create([
        'name'=>$request->name,
        'color'=>$request->color,
        'user_id'=>$request->user_id,
    ]);
    return response($tag);
});

// Поиск определенного тега

Route::get('/tag/{id}', function($id) {
    return response(Tag::find($id));
});

// Все заметки

Route::get('/notes', function() {
    return response(Note::all());
});

// Добавление заметок

Route::post('/note/add', function(Request $request) {
    $note = Note::create([
        'name'=>$request->name,
        'text'=>$request->text,
        'team_id'=>$request->team_id,
    ]);
    return response($note);
});

// поиск Заметки по ID

Route::get('/note/{id}', function($id) {
    return response(Note::find($id));
});

// Все пользователи

Route::get('/users', function() {
    return response(User::all());
});

// Добавление пользователя

Route::post('/user/add', function(Request $request) {
    $user = User::create([
        'name'=>$request->name,
        'email'=>$request->email,
        'password'=>$request->password,
    ]);
    return response($user);
});

// Поиск пользователя по ID

Route::get('/user/{id}', function($id) {
    return response(User::find($id));
});

// Все команды

Route::get('/teams', function() {
    return response(Team::all());
});

// Добавление команды

Route::post('/team/add', function(Request $request) {
    $team = Team::create(['name'=>$request->name]);
    return response($team);
});

// поиск Команды по ID

Route::get('/team/{id}', function($id) {
    return response(Team::find($id));
});

// все tag_note

Route::get('/tag_notes', function() {
    return response(Tag_Note::all());
});

// Создание tag_note

Route::post('/tag_note/add', function(Request $request) {
    $tag_note = Tag_Note::create([
        'tag_id'=>$request->tag_id,
        'note_id'=>$request->note_id,
        ]);
    return response($tag_note);
});

// Поиск определенного tag_note`a

Route::get('/tag_note/{id}', function($id) {
    return response(Tag_Note::find($id));
});

// Все user_team`ы

Route::get('/user_teams', function() {
    return response(User_Team::all());
});

// Добавление user_team

Route::post('/user_team/add', function(Request $request) {
    $user_team = User_Team::create([
        'user_id'=>$request->user_id,
        'team_id'=>$request->team_id,
        ]);
    return response($user_team);
});

// поиск user_team

Route::get('/user_team/{id}', function($id) {
    return response(User_Team::find($id));
});
